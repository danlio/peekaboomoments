let reptile_main = new function () {
  let $this = this;
  let axios = require("axios");
  let qs = require('qs');
  let fs = require('fs');
  let path = require('path');
  let piexif = require("piexifjs");
  let instance = axios.create();
  let moment = require('moment');
  let os = require('os');

  require('dotenv').config({ path: process.cwd() + '/src/.env'});

  let WebHeader = {
    'content-type':  'application/x-www-form-urlencoded',
    'user-agent':  'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36',
    'cookie' : ''
  }
  /**
   * @ = baby id
   * # = since id
   */
  $this.urlMap = {
        domain: "https://www.peekaboomoments.com",
        login: "/users/sign_in.json",
        event: "/events?baby_id=@&style=best_12&include_rt=true",
        auth: "/moments/new?force_aliyun=true",
        data: "/events/updates?baby_id=@&style=best_12&since=#&skip_invisible=true&include_rt=true",
      };
  $this.data = {};
  $this.tidyData = {};
  $this.totalCount = 0;
  // 登入
  $this.loginWeb = function (user, res = {}) {
    console.log(user)
    instance(
      {
        url: $this.urlMap.domain + $this.urlMap.login,
        method: 'post',
        headers: WebHeader,
        data: qs.stringify({user}),
      },
      {
        timeout: process.env['CURL_TIMEOUT']
      }
    ).then(function (response) {
        WebHeader.cookie = response.headers['set-cookie'];
        console.log(response.data.data)
        let output = {
          code: "200",
          data: response.data.babies
        }
        res.end(JSON.stringify(output));
    })
    .catch(function (error) {
      res.end(JSON.stringify(error.response.data));
    });
  }
  // 呼叫一次 event api 取得 baby 資料
  $this.getWeb = function (eventUrl, res = {}) {
    $this.data = {};
    $this.tidyData = {};
    $this.totalCount = 0;
    instance(
    {
      url: $this.urlMap.domain + eventUrl,
      method: 'get',
      headers: WebHeader,
    },
    {
      timeout: process.env['CURL_TIMEOUT']
    }
    ).then(function (response) {
      $this.getData(response.data.list[0].baby_id, 0, res);
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  // 抓取 json 資料
  $this.getData = function (baby_id, since, res) {
    let dataUrl = $this.urlMap.data.replace("@",baby_id).replace("#",since);
    instance(
      {
        url: $this.urlMap.domain + dataUrl,
        method: 'get',
        headers: WebHeader,
      },
      {
        timeout: process.env['CURL_TIMEOUT']
      }
    ).then(function (response) {
      if(response.data.list.length != 0) {
        if (since != 0) {
          $this.data[since] = response.data.list;
          $this.getData(baby_id,response.data.list.pop().updated_at_in_ts, res);
        } else {
          $this.getData(baby_id,response.data.list.pop().updated_at_in_ts, res);
        }
      } else {
        console.log('data get end');
        setData(baby_id, res);
      }
    })
    .catch(function (error) {
      console.log(error);
      console.log(baby_id,since);
    });
  }
  // 整理抓回來的 json
  let setData = function (baby_id, res = {}) {
    for (let since in $this.data) {
      for (let key in $this.data[since]) {
        let layout_detail = $this.data[since][key].layout_detail;
        for (let layout_key in layout_detail) {
          let downUrl = '';
          if(layout_detail[layout_key].created_at != null) {
            let downPath = layout_detail[layout_key].created_at.split('T')[0];
            switch(layout_detail[layout_key].type) {
              case "video": {
                  if(layout_detail[layout_key].video_path == null) {
                    downUrl = layout_detail[layout_key].original_video_path;
                  } else {
                    downUrl = layout_detail[layout_key].video_path;
                  }
              }break
              case "picture": {
                downUrl = layout_detail[layout_key].picture;
              }break;
            }
            GetTidyData(downUrl, process.env['DOWNLOAD_PATH'] + downPath, layout_detail[layout_key]);
          }
        }
      }
    }
    TidyNowData(baby_id, res);
  }
  // 取代網址
  let urlReplace = function (url) {
    let map = ['alicnssl'];
    let newUrl = '';
    for (let key in map) {
      newUrl = url.replace(map[key], 'alihk')
    }
    return newUrl;
  }
  // 刪除空 object 和 排序
  let TidyNowData = function (baby_id, res = {}) {
    for (let baby in $this.tidyData) {
      for (let key in $this.tidyData[baby]) {
        for (let key2 in $this.tidyData[baby][key]) {
          if ($this.tidyData[baby][key][key2].length == 0 ) {
            delete $this.tidyData[baby][key][key2];
          }
        }
      }
    }
    console.log("Total data : " + $this.totalCount);
    WebGetData(baby_id, $this.tidyData[baby_id.toString()], res);
  }
  // 整理資料且塞入 EXIF資料
  let GetTidyData = function(fileUrl, downloadFolder, data) {
    if(fileUrl == null){
      console.log(data)
    } else {
      $this.totalCount++;
    }
    let folderMap = downloadFolder.split('/');
    let year_mon = folderMap[1].split("-")[0] + "-" + folderMap[1].split("-")[1];

    let Timestamp = moment(data.created_at).valueOf();
    let tidyData = {
      url: fileUrl,
      created_at: moment(Timestamp).format('YYYY-MM-DD HH:mm:ss'),
      source: data.source,
      type: data.type,
      preview: urlReplace(data.picture)
    }
    if ($this.tidyData[data.baby_id]) {
      if ($this.tidyData[data.baby_id][year_mon]) {
        if ($this.tidyData[data.baby_id][year_mon][folderMap[1]]) {
          $this.tidyData[data.baby_id][year_mon][folderMap[1]].push(tidyData);
        } else {
          $this.tidyData[data.baby_id][year_mon][folderMap[1]] = [];
          $this.tidyData[data.baby_id][year_mon][folderMap[1]].push(tidyData);
        }
      } else {
        $this.tidyData[data.baby_id][year_mon] = {};
        $this.tidyData[data.baby_id][year_mon][folderMap[1]] = [];
        $this.tidyData[data.baby_id][year_mon][folderMap[1]].push(tidyData);
      }
    } else {
      $this.tidyData[data.baby_id] = {};
      $this.tidyData[data.baby_id][year_mon] = {};
      $this.tidyData[data.baby_id][year_mon][folderMap[1]] = [];
      $this.tidyData[data.baby_id][year_mon][folderMap[1]].push(tidyData);
    }
  }
  // 下載 檔案
  $this.downloadFile = async (fileUrl, downloadFolder, data) => {
    let fileName = path.basename(fileUrl);
    let folderMap = downloadFolder.split('/');
    let nowfolder = '';
    for (let key in folderMap) {
      nowfolder += folderMap[key] + "/";
      if (!fs.existsSync(nowfolder)) {
        fs.mkdirSync(nowfolder);
        console.log("mkdir : " + nowfolder,key)
      }
    }
    let localFilePath = path.resolve(os.homedir() + "/Desktop/", downloadFolder, fileName);
    let response = await instance(
      {
        method: 'GET',
        url: fileUrl,
        responseType: 'stream',
        'user-agent': WebHeader['user-agent'],
        cookie: WebHeader.cookie
      },
      {
        timeout: process.env['CURL_TIMEOUT']
      }
    ).then((response) => {
      return response;
    }).catch((error) => {
      console.log("curl Url : " + fileUrl)
      return error;
    });
    let w = response.data.pipe(fs.createWriteStream(localFilePath));
    return new Promise((resolve, reject) => {
      w.on("finish", function (){
        console.log('finish : ' + fileUrl);
        if (data.type == 'picture') {
            modifyEXIF(localFilePath, data);
        }
        resolve();
      });
      w.on("error", function (error){
          console.log(error);
          reject();
      });
    });
  }
  // 圖片 EXIF 寫入
  let modifyEXIF = async (filePath, filedata) => {
    let jpeg = fs.readFileSync(filePath);
    let data = jpeg.toString("binary");
    let zeroth = {};
    let exif = {};
    zeroth[piexif.ImageIFD.Make] = process.env['IMAGE_MAKE'];
    zeroth[piexif.ImageIFD.Software] = process.env['IMAGE_SOFTWARE'];
    zeroth[piexif.ImageIFD.DateTime] = filedata.created_at;
    zeroth[piexif.ImageIFD.Model] = filedata.source;
    exif[piexif.ExifIFD.DateTimeOriginal] = filedata.created_at;
    exif[piexif.ExifIFD.DateTimeDigitized] = filedata.created_at;
    exif[piexif.ExifIFD.LensMake] = filedata.source;
    let exifObj = {"0th": zeroth, "Exif": exif};
    let exifbytes = piexif.dump(exifObj);
    let newData = piexif.insert(exifbytes, data);
    let newJpeg = Buffer.from(newData, "binary");
    fs.writeFileSync(filePath, newJpeg);
    console.log("modify EXIF : " + filePath);
  }
  // res get data
  let WebGetData = function(id, data , res) {
    let output = {
      'id': id,
      'data': data
    }
    res.end(JSON.stringify(output));
  }
  // 初始
  $this.init = function () {
    console.log("Reptile Ready");
    instance.defaults.timeout = process.env['CURL_TIMEOUT'];
  }
  $this.init();
}

let web_server = new function() {
  let $this = this;
  let express = require("express");
  let http = express();
  let bodyParser = require('body-parser');
  let urlencodedParser = bodyParser.urlencoded({ extended: false });
  let os = require('os');
  require('dotenv').config({ path: process.cwd() + '/src/.env'});
  // web server
  let WebServer = function () {
    http.use('/src', express.static('src'));

    http.get("/", (req, res) => {
      res.sendFile(process.cwd() + "/src/resources/view/index.html");
    });
    http.post("/login", urlencodedParser, (req, res) => {
      reptile_main.loginWeb(req.body , res);
    });
    http.post("/getWeb", urlencodedParser, (req, res) => {
      reptile_main.getWeb(reptile_main.urlMap.event.replace("@",req.body.id) ,res);
    });
    http.post("/download", urlencodedParser, (req, res) => {
      let post_data = req.body['file[]'];
      let post_id = req.body.id;
      if (typeof post_data == "string") {
        post_data = [post_data];
      }
      for(let i in post_data) {
        let map = post_data[i].split("_");
        let data = {
          id: map[0],
          Ym: map[1],
          Ymd: map[2],
          key: map[3]
        }
        reptile_main.downloadFile(reptile_main.tidyData[data.id][data.Ym][data.Ymd][data.key].url, os.homedir() + "/Desktop/" + process.env['DOWNLOAD_PATH'] + data.id + '/' + data.Ym, reptile_main.tidyData[data.id][data.Ym][data.Ymd][data.key]);
      }
      let output = {"code": 200, "msg": os.homedir() + "/Desktop/" + process.env['DOWNLOAD_PATH']};
      res.end(JSON.stringify(output));
    });
  }
  $this.init = function() {
    WebServer();
    http.listen(process.env['WEB_PORT']);
    console.log("Web Server Ready");
  }
  $this.init();
}