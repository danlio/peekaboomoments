let webUI = new function() {
  $this = this;
  let UserData = {};
  let babiesData = {};
  let babiesID = '';
  let buttonEvent = function () {
    $(".cls_submit").on('click', () => {
      $(".cls_login").each((function () {
        UserData[$(this).attr("name")] = $(this).val();
      }));
      $('.loading').show();
      $.ajax(
        {
          url: window.location.href + "login",
          type: 'POST',
          data: UserData,
          success: function(resp) {
            let data = JSON.parse(resp);
            if (data.code == '200') {
              $this.CreateBabiesView(data.data);
            } else {
              $('.loading').hide();
              alert(data.error);
            }
          }
        }
      )
    });
    $(".cls_btn_download").on("click", () => {
      let data = [];
      let id = '';
      $(".cls_checkbox").each(function(){
        if ($(this).prop("checked")) {
          let val = $(this).attr("data");
          id = $(this).attr("data").split("_")[0];
          data.push(val);
        }
      });
      if (data.length != 0) {
        $('.loading').show();
        $.ajax(
          {
            url: window.location.href + "download",
            type: 'POST',
            data: { 'id': id,'file': data},
            success: function(resp) {
              let data = JSON.parse(resp);
              //alert("背景下載至 " + data.msg );
              $('.loading').hide();
            }
          }
        )
      }
    });
  }
  $this.CreateYmdImg = function (id) {
    let id_map = id.split('_');
    let sData = {};
    if (id_map.length == 3) {
      sData.babies = id_map[0];
      sData.Ym = id_map[1];
      sData.Ymd = id_map[2];
      let data = babiesData;
      let imgData = data.get(sData.Ym).get(sData.Ymd);
      imgData.forEach((img, key) => {
        let img_id = id + "_" + key
        $("#"+img_id).attr(
          {
            "src": img.preview + "!xlarge"
          }
        )
      });
      $("#" + id).attr({"change-type": "true"});
    }
  }
  $this.CreateBabiesData = function (baby_id, data) {
    babiesID = baby_id;
    $(".cls_list").html("");
    let div_accordion = $("<div/>").attr({id:"accordion"});
    data.forEach((subData,Ym) => {
      let div_card = $("<div/>").addClass("card");
      let a_obj = $("<a/>").addClass("collapsed").attr(
        {
          "role": "button",
          "data-toggle": "collapse",
          "aria-expanded": "false",
          "href": "#collapse-" + Ym,
          "aria-control": "collapse-" + Ym
        }
      )
      .append($("<span/>").text(Ym))
      .append(
        $("<span/>")
          .addClass("badge bg-success text-dark")
          .css({"margin-left": "15px"})
          .text(data.get(Ym).size)
        )
      let div_header = $("<div/>").addClass("card-header").attr(
        {
          "id": "heading-" + Ym
        }
      );
      let div_check = $("<div/>")
        .addClass("form-check")
          .append(
            $("<input/>")
              .addClass("form-check-input")
              .attr(
                {
                  "id": Ym,
                  "type": "checkbox"
                }
              ).on("change", function () {
                $(".cls_" + Ym).click();
              })
          )
      let h5 = $("<h5/>").addClass("mb-0").append(a_obj);
      div_header.append(div_check.append(h5));
      let div_coolapse = $("<div/>").addClass("collapse").attr(
        {
          "id": "collapse-" + Ym,
          "data-parent": "#accordion",
          "aria-labelledby": "heading-" + Ym
        }
      );
      let div_card_body = $("<div/>").addClass("card-body");
      let div_accordion_sub = $("<div/>").attr({id:"accordion-" + Ym});
      subData.forEach((subDataImg, Ymd) => {
        let div_card_sub = $("<div/>").addClass("card");
        let a_obj_sub = $("<a/>").addClass("collapsed").attr(
          {
            "role": "button",
            "data-toggle": "collapse",
            "aria-expanded": "false",
            "href": "#collapse-" + Ymd,
            "aria-control": "collapse-" + Ymd,
            "id": baby_id + "_" + Ym + "_" + Ymd,
            "change-type": "false"
          }
        ).append(
          $("<span/>").text(Ymd)
        ).append(
          $("<span/>")
            .addClass("badge bg-success text-dark")
            .css({"margin-left": "15px"})
            .text(subData.get(Ymd).size)
          )
        .on("click" , function() {
          console.log("sub a click ",$(this).attr("id"), $(".cls_" + Ym).prop("checked"));
          $this.CreateYmdImg($(this).attr("id"));
        });
        let div_check_sub = $("<div/>")
        .addClass("form-check")
          .append(
            $("<input/>")
              .addClass("form-check-input cls_" + Ym)
              .attr(
                {
                  "id": Ymd,
                  "type": "checkbox"
                }
              ).on("change", function () {
                $(".cls_" + Ymd).click();
              })
          )
        let div_header_sub = $("<div/>").addClass("card-header").attr(
          {
            "id": "heading-" + Ymd
          }
        );
        let h5_sub = $("<h5/>").addClass("mb-0").append(a_obj_sub);
        div_header_sub.append(div_check_sub.append(h5_sub));
        let div_coolapse_sub = $("<div/>").addClass("collapse").attr(
          {
            "id": "collapse-" + Ymd,
            "data-parent": "#accordion-" + Ym,
            "aria-labelledby": "heading-" + Ymd
          }
        );
        let div_img = $("<div/>").addClass("row");
        subDataImg.forEach((imgData, key) => {
          let img = $("<img/>")
            .addClass("img-thumbnail rounded")
            .attr(
              {
                //"src": data[Ym][Ymd][key].preview
                "id": baby_id + "_" + Ym + "_" + Ymd + "_" + key
              }
            ).on("click", function (){
              console.log("image click ",$(this).attr("id"));
            });
            let div_check_img = $("<div/>")
              .addClass("form-check")
                .append(
                  $("<input/>")
                    .addClass("form-check-input cls_" + Ymd + ' cls_checkbox')
                    .attr(
                      {
                        "id": "checkbox_" + Ym + "_" + Ymd + "_" + key,
                        "type": "checkbox",
                        "data":  baby_id + "_" + Ym + "_" + Ymd + "_" + key
                      }
                    )
                );
            let img_msg = $("<div/>").append(
              $("<span/>")
                .addClass("badge bg-info text-dark")
                .text("type : " + imgData.type)
              )
          div_img.append($("<div/>").addClass("col-md-2").append(div_check_img.append(img).append(img_msg)))
        });
        let div_card_body_sub = $("<div/>").addClass("card-body").append(div_img);
        div_accordion_sub.append(div_card_sub.append(div_header_sub).append(div_coolapse_sub.append(div_card_body_sub)));
      });
      div_accordion.append(div_card.append(div_header).append(div_coolapse.append(div_card_body.append(div_accordion_sub))));
    });
    $(".cls_list").append(div_accordion);
    $this.fancybox();
    $('.loading').hide();
  }
  let objToMap = function(obj) {
    let tidyData = obj;
    for (let Ym in tidyData) {
      for(let Ymd in tidyData[Ym]) {
        tidyData[Ym][Ymd] = new Map (Object.entries(tidyData[Ym][Ymd]).sort().reverse());
      }
      tidyData[Ym] = new Map (Object.entries(tidyData[Ym]).sort().reverse());
    }
    tidyData = new Map (Object.entries(tidyData).sort().reverse());
    babiesData = tidyData;
    return tidyData;
  }
  $this.CreateBabiesView = function (data) {
    let ul_row = $(".cls_ul");
    ul_row.html("");
    for (let key in data) {
      let li = $("<li/>").addClass("col-12 col-md-6 col-lg-3").attr({"id":data[key].id})
      let div = $("<div/>").addClass("cnt-block equal-hight").css({"height":"300px"});
      let figure = $("<figure/>");
      let img = $("<img>").addClass("img-responsive").attr({src:data[key].avatar.original});
      figure.append(img);
      let h3 = $("<h3/>").text(data[key].name);
      iconID = $("<i/>").addClass("fas fa-user").text("：" + data[key].id);
      let p = $("<p/>").append(iconID).append($("<br/>"));
      iconBirthday = $("<i/>").addClass("fas fa-birthday-cake").text("：" + data[key].birthday);
      h3.append(p.append(iconBirthday));
      li.on("click",function() {
        let baby_id = $(this).attr("id");
        $('.loading').show();
        $.ajax(
          {
            url: window.location.href + "getWeb",
            type: 'POST',
            data: {'id': baby_id},
            success: function(resp) {
              //resp data 需要轉map 在丟給 CreateBabiesData
              let data = objToMap(JSON.parse(resp).data);
              $this.CreateBabiesData(baby_id, data);
            }
          }
        )
        //reptile_main.getWeb(reptile_main.urlMap.event.replace("@",$(this).attr("id")));
      });
      ul_row.append(li.append(div.append(figure).append(h3)));
    }
    $("#login").hide();
    $(".cls_babies_box").show();
    $('.loading').hide();
  }
  $this.fancybox = function() {
    $.fancybox.open({
      src  : '.cls_box_data',
      type : 'inline',
      iframe : {
        css : {
          width  : '800px',
          height : '600px'
        }
      },
      opts : {
        afterShow : function( instance, current ) {
          console.info( 'done!' );
        },
        afterClose : function() {
          //$(".cls_list").html("");
          console.log("fancybox close")
        }
      }
    });
  }
  $this.init = function () {
    buttonEvent();
    $(".cls_babies_box").hide();
    $(".cls_box_data").hide();
  }
  $this.init();
}