# 時光小屋備份


## 打包應用 
```
clone https://gitlab.com/danlio/peekaboomoments.git

cd peekaboomoments

npm install

npm run prod

```

打包完後再 peekaboomoments 資料夾底下會出現 `dist/peekaboomoments_backup`
再分別個 系統使用 資料夾
| OS | 執行檔 |
|----|-------|
|linux32 | peekaboomoments_backup |
|linux64 | peekaboomoments_backup |
|osx64   | peekaboomoments_backup |
|win32   | peekaboomoments_backup.exe |
|win64   | peekaboomoments_backup.exe |

---
# 注意 

`備份下來的圖片＆影片會在桌面 peekaboomoments_backup 資料夾內`

`登入帳號需使用 時光小屋帳密`
